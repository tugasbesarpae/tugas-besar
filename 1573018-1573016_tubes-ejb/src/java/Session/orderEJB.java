/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.*;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author 
 */
@Stateless
public class orderEJB implements orderEJBRemote {

    @PersistenceContext(unitName = "1573018-1573016_tubes-ejbPU")
    private EntityManager em;

    @Override
    public List<pesanan> findPesanan() {
        Query q = em.createNamedQuery("findAllPesanan");
        return q.getResultList();
    }

    @Override
    public List<detailPesanan> findDetailDariPesanan(int id) {
        Query q = em.createQuery("SELECT p FROM detailPesanan p WHERE p.idOrder = " + id);
        return q.getResultList();
    }

    @Override
    public pesanan findPesananById(int id) {
        return em.find(pesanan.class, id);
    }

    @Override
    public List<pesanan> findPesananCustomer(int id ){
        List<pesanan> daftar = new ArrayList<>();
        Query q = em.createQuery("SELECT p FROM pesanan p WHERE p.idCustomer = " + id);
        daftar = q.getResultList();
        return daftar;
    }
    @Override
    public pesanan createPesanan(pesanan b) {
        em.persist(b);
        return b;
    }

    @Override
    public void deletePesanan(pesanan b) {
        em.remove(em.merge(b));
    }

    @Override
    public pesanan updatePesanan(pesanan b) {
        return em.merge(b);
    }
}
