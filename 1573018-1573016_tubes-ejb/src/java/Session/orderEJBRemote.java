/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.detailPesanan;
import Entity.pesanan;
import java.util.List;
import javax.ejb.Remote;
import javax.persistence.Query;

/**
 *
 * @author
 */
@Remote
public interface orderEJBRemote {

    public List<pesanan> findPesanan();

    public List<detailPesanan> findDetailDariPesanan(int id);

    public pesanan findPesananById(int id);
    
    public pesanan createPesanan(pesanan b);

    public void deletePesanan(pesanan b);

    public pesanan updatePesanan(pesanan b);
    
    public List<pesanan> findPesananCustomer(int id );
}
