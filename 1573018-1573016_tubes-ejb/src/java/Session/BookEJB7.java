/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.book;
import java.util.List;
import javax.ejb.*;
import javax.persistence.*;

/**
 *
 * @author
 */
//@LocalBean
@Stateless
public class BookEJB7 implements BookEJBRemote7 {

    @PersistenceContext(unitName = "1573018-1573016_tubes-ejbPU")
    private EntityManager em;

    @Override
    public List<book> findBooks() {
        Query q = em.createNamedQuery("findAllBooks");
        return q.getResultList();
    }

    @Override
    public book findBookByTitle(String title) {
        Query q = em.createQuery("SELECT b FROM book b WHERE b.title = '" + title + "'");
        return (book) q.getResultList().get(0);
    }

    @Override
    public book findBookById(Long id) {
        return em.find(book.class, id);
    }

    @Override
    public book createBook(book b) {
        em.persist(b);
        return b;
    }

    @Override
    public void deleteBook(book b) {
        em.remove(em.merge(b));
    }

    @Override
    public book updateBook(book b) {
        return em.merge(b);
    }
}
