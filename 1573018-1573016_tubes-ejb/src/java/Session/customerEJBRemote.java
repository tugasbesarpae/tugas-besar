/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.*;
import java.util.List;
import javax.ejb.Remote;
import javax.persistence.Query;

/**
 *
 * @author 
 */
@Remote
public interface customerEJBRemote {
    
    public List<pelanggan> findCustomer();

    public pelanggan findCustomerById(int id);

    public pelanggan createCustomer(pelanggan b);

    public void deleteCustomer(pelanggan b);

    public pelanggan updateCustomer(pelanggan b);
    
    public pelanggan validatePelangganLoginDetails(int id, String pw) throws Exception;
}
