/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.book;
import Entity.detailPesanan;
import Entity.pelanggan;
import Entity.pesanan;
import java.util.List;
import javax.ejb.Remote;
import javax.ejb.Remove;

/**
 *
 * @authora
 */
@Remote
public interface cartSessRemote {

    public void addItem(book b);

    public void removeItem(detailPesanan b);

    public List<book> getCartBooks();

    public Float getTotal();

    public List<detailPesanan> getListDetail();

    public void setListDetail(List<detailPesanan> listDetail);

    public int checkout(pelanggan pe);

    public void addKuantitasDalamDetail(detailPesanan b);

    public void kurangiKuantitasDalamDetail(detailPesanan b);

    public void empty();

}
