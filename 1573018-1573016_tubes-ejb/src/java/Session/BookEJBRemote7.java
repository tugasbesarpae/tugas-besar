/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.book;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author
 */
@Remote
public interface BookEJBRemote7 {

    public List<book> findBooks();

    public book findBookById(Long id);

    public book createBook(book b);

    public void deleteBook(book b);

    public book updateBook(book b);

    public book findBookByTitle(String title);
}
