/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.*;
import javax.ejb.*;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author
 */
@Stateful
public class cartSessBean implements cartSessRemote {

    @PersistenceContext(unitName = "1573018-1573016_tubes-ejbPU")
    private EntityManager em;

    private List<book> cartBooks = new ArrayList<>();
    private List<detailPesanan> listDetail = new ArrayList<>();
    private int kuantitas = 1;

    @Override
    public void addItem(book b) {
        try {
            boolean isBelumAda = true;
            if (listDetail.isEmpty()) {
                addDetailPesanan(b);
                throw new Exception();
            } else {
                for (detailPesanan dp : listDetail) {
                    if (dp.getId().getId().equals(b.getId())) {
                        isBelumAda = false;
                        break;
                    } else {
                        isBelumAda = true;
                    }
                }
            }

            if (isBelumAda) {
                addDetailPesanan(b);
            } else {
                addKuantitas(b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addDetailPesanan(book b) {
        detailPesanan dp = new detailPesanan();
        dp.setId(b);
        dp.setHargaBeli(b.getPrice());
        dp.setKuantitas(1);
        listDetail.add(dp);
    }

    public void addKuantitas(book b) {
        for (detailPesanan dp : listDetail) {
            if (dp.getId().getId().equals(b.getId())) {
                dp.setKuantitas(kuantitas + dp.getKuantitas());

            }
        }
    }

    @Override
    public void addKuantitasDalamDetail(detailPesanan b) {
        for (detailPesanan dp : listDetail) {
            if (dp.getId().getId().equals(b.getId().getId())) {
                dp.setKuantitas(kuantitas + dp.getKuantitas());
            }
        }
    }

    @Override
    public void kurangiKuantitasDalamDetail(detailPesanan b) {
        for (detailPesanan dp : listDetail) {
            if (dp.getId().getId().equals(b.getId().getId())) {
                dp.setKuantitas(dp.getKuantitas() - kuantitas);
            }
        }
    }

    @Override
    public void removeItem(detailPesanan b) {
        try{
        for (detailPesanan dp : listDetail) {
            if (dp.getIdDetailOrder() == b.getIdDetailOrder()) {
                listDetail.remove(dp);
            }
        }
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Float getTotal() {
        if (listDetail.isEmpty()) {
            return 0f;
        }
        Float total = 0f;
        for (detailPesanan b : listDetail) {
            total += (b.getId().getPrice() * b.getKuantitas());
        }
        return total;
    }

    @Override
    public int checkout(pelanggan pe) {
        pesanan p = new pesanan();
        p.setIdCustomer(pe);
        Float totalHarga = 0f;
        for (detailPesanan b : listDetail) {
            b.setIdOrder(p);
            totalHarga += b.getId().getPrice();
        }

        p.setDaftarDetailPesanan(listDetail);
        p.setTotalHarga(totalHarga);
        em.persist(p);

        empty();
        return p.getIdOrder();
    }

    @Override
    public void empty() {
        cartBooks.clear();
        listDetail.removeAll(listDetail);
    }

    //getter-setter
    @Override
    public List<book> getCartBooks() {
        return cartBooks;
    }

    public int getKuantitas() {
        return kuantitas;
    }

    public void setKuantitas(int kuantitas) {
        this.kuantitas = kuantitas;
    }

    @Override
    public List<detailPesanan> getListDetail() {
        return listDetail;
    }

    @Override
    public void setListDetail(List<detailPesanan> listDetail) {
        this.listDetail = listDetail;
    }

    public void setCartBooks(List<book> cartBooks) {
        this.cartBooks = cartBooks;
    }

}
