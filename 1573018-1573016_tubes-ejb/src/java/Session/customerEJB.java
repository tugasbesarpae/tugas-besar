/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.*;
import java.util.*;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author
 */
@Stateless
public class customerEJB implements customerEJBRemote {

    @PersistenceContext(unitName = "1573018-1573016_tubes-ejbPU")
    private EntityManager em;

    @Override
    public List<pelanggan> findCustomer() {
        Query q = em.createNamedQuery("findAllCustomers");
        return q.getResultList();
    }

    @Override
    public pelanggan findCustomerById(int id) {
        return em.find(pelanggan.class, id);
    }

    @Override
    public pelanggan validatePelangganLoginDetails(int id, String pw) throws Exception {
        pelanggan pVal = em.find(pelanggan.class, id);
        if (pVal.getPassword().equals(pw)) {
            return pVal;
        } else {
            throw new Exception();
        }
    }

    @Override
    public pelanggan createCustomer(pelanggan b) {
        em.persist(b);
        return b;
    }

    @Override
    public void deleteCustomer(pelanggan b) {
        em.remove(em.merge(b));
    }

    @Override
    public pelanggan updateCustomer(pelanggan b) {
        return em.merge(b);
    }
}
