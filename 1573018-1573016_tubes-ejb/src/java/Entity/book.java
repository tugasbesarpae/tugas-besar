/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author
 */
@Entity
@NamedQuery(name = "findAllBooks", query = "SELECT b FROM book b")
public class book implements Serializable {

    @TableGenerator(
            name = "generatorInTable",
            allocationSize = 1,
            initialValue = 1
    )
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "generatorInTable")
    private Long id;
    @Column(nullable = false)
    private String title;
    private Float price;
    @Column(length = 2000)
    private String Description;
    private String isbn;
    private Integer nbOfPage;
    private Boolean illustrations;

    private String gambar;
    private String link;

    @OneToMany(mappedBy = "id",
            cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<detailPesanan> daftarDetailPesanan = new ArrayList<>();

    public book() {
    }

    public book(Long id, String title, Float price, String Description, String isbn, Integer nbOfPage, Boolean illustrations) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.Description = Description;
        this.isbn = isbn;
        this.nbOfPage = nbOfPage;
        this.illustrations = illustrations;
    }

    //getter-setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getPrice() {
        return price;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<detailPesanan> getDaftarDetailPesanan() {
        return daftarDetailPesanan;
    }

    public void setDaftarDetailPesanan(List<detailPesanan> daftarDetailPesanan) {
        this.daftarDetailPesanan = daftarDetailPesanan;
    }

    public Integer getNbOfPage() {
        return nbOfPage;
    }

    public void setNbOfPage(Integer nbOfPage) {
        this.nbOfPage = nbOfPage;
    }

    public Boolean getIllustrations() {
        return illustrations;
    }

    public void setIllustrations(Boolean illustrations) {
        this.illustrations = illustrations;
    }

}
