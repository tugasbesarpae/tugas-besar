/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author
 */
@Entity
@NamedQuery(name = "findAllCustomers", query = "SELECT b FROM pelanggan b")
public class pelanggan implements Serializable {

  @TableGenerator(
            name = "generatorInTable",
            allocationSize = 1,
            initialValue = 1
    )
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "generatorInTable")
    private int idCustomer;
    private String password;

    private String nama;
    private String alamat;
    private String kota;
    private String email;
    private int telepon;

    @OneToMany(mappedBy = "idCustomer",
            cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<pesanan> daftarPesanan = new ArrayList<>();

    public pelanggan() {
    }

    //getter-setter
    public int getIdCustomer() {
        return idCustomer;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIdCustomer(int idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTelepon() {
        return telepon;
    }

    public void setTelepon(int telepon) {
        this.telepon = telepon;
    }

    public List<pesanan> getDaftarPesanan() {
        return daftarPesanan;
    }

    public void setDaftarPesanan(List<pesanan> daftarPesanan) {
        this.daftarPesanan = daftarPesanan;
    }

}
