/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author
 */
@Entity
@NamedQuery(name = "findAllDetailPesanan", query = "SELECT b FROM detailPesanan b")
public class detailPesanan implements Serializable {

    @TableGenerator(
            name = "generatorInTable",
            allocationSize = 1,
            initialValue = 1
    )
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "generatorInTable")
    private int idDetailOrder;

    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name = "idOrder")
    private pesanan idOrder;

    private book id;

    private int kuantitas;

    private Float hargaBeli;

    public detailPesanan() {
    }

    //getter-setter
    public int getIdDetailOrder() {
        return idDetailOrder;
    }

    public void setIdDetailOrder(int idDetailOrder) {
        this.idDetailOrder = idDetailOrder;
    }

    public pesanan getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(pesanan idOrder) {
        this.idOrder = idOrder;
    }

    public book getId() {
        return id;
    }

    public int getKuantitas() {
        return kuantitas;
    }

    public void setKuantitas(int kuantitas) {
        this.kuantitas = kuantitas;
    }

    public void setId(book id) {
        this.id = id;
    }

    public Float getHargaBeli() {
        return hargaBeli;
    }

    public void setHargaBeli(Float hargaBeli) {
        this.hargaBeli = hargaBeli;
    }

}
