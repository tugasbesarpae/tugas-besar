/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entity.*;
import Session.*;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;
import javax.ejb.EJB;

/**
 *
 * @author Developer
 */
@Named(value = "generalBean")
@SessionScoped
public class generalBean implements Serializable {

    //Bean ini digunakan untuk kegunaan umum, ex : testing, dsb.
    //TIDAK UNTUK dipakai di JSF
    
    //EJB Remote Instantiation
    @EJB
    private BookEJBRemote7 bookEjb;
    @EJB
    private customerEJBRemote customerEjb;

    //@PersistenceContext(unitName = "1573018-1573016_tubes-ejbPU")
    //private EntityManager em;
    //General : pemakaian umum, ex : findAll, dsb. 
    //CRUD    : digunakan untuk proses Create, Update, Find, dan Delete.
    //----- Book -----
    //General
    private List<book> listAllBook;
    //CRUD
    private book bookGeneral;

    //----- Customer -----
    //Login Data
    private pelanggan loggedPelanggan;
    //CRUD
    private pelanggan pelangganGeneral;

    //----- Pesanan -----
    //General
    private List<pesanan> listAllPesanan;
    //CRUD
    private pesanan pesananGeneral;

    //----- Detail Pesanan -----
    //General
    private List<detailPesanan> listAllDetailPesanan;
    //CRUD
    private detailPesanan dpGeneral;

    public generalBean() {
        //Ambil pelangganYangLogin
        loggedPelanggan = customerBean.pelangganYangLogin;
        //List Instantiation
        listAllBook = new ArrayList<>();  
        listAllPesanan = new ArrayList<>();
        listAllDetailPesanan = new ArrayList<>();
        //General Instantiation
        bookGeneral = new book();
        pelangganGeneral = new pelanggan();
        pesananGeneral = new pesanan();
        dpGeneral = new detailPesanan();
    }

    //Book CRUD Methods
    //Read All
    public void readAllBooks() {
        listAllBook = bookEjb.findBooks();
    }

    public String testSpawn(){
        return "tester";
    }
    //Find
    public book findBook() {
        return bookGeneral = bookEjb.findBookById(bookGeneral.getId());
    }
    //Add
    //Update
    //Delete

    //Customer CRUD Methods
    //Read All
    //Find
    //Add
    //Update
    //Delete
    //Pesanan CRUD Methods
    //Read All
    //Find
    //Add
    //Update
    //Delete
    //Detail Pesanan CRUD Methods
    //Read All
    //Find
    //Add
    //Update
    //Delete
    //getter-setter
    public List<book> getListAllBook() {
        return listAllBook;
    }

    public void setListAllBook(List<book> listAllBook) {
        this.listAllBook = listAllBook;
    }

    public book getBookGeneral() {
        return bookGeneral;
    }

    public void setBookGeneral(book bookGeneral) {
        this.bookGeneral = bookGeneral;
    }

    public pelanggan getLoggedPelanggan() {
        return loggedPelanggan;
    }

    public void setLoggedPelanggan(pelanggan loggedPelanggan) {
        this.loggedPelanggan = loggedPelanggan;
    }

    public pelanggan getPelangganGeneral() {
        return pelangganGeneral;
    }

    public void setPelangganGeneral(pelanggan pelangganGeneral) {
        this.pelangganGeneral = pelangganGeneral;
    }

    public List<pesanan> getListAllPesanan() {
        return listAllPesanan;
    }

    public void setListAllPesanan(List<pesanan> listAllPesanan) {
        this.listAllPesanan = listAllPesanan;
    }

    public pesanan getPesananGeneral() {
        return pesananGeneral;
    }

    public void setPesananGeneral(pesanan pesananGeneral) {
        this.pesananGeneral = pesananGeneral;
    }

    public List<detailPesanan> getListAllDetailPesanan() {
        return listAllDetailPesanan;
    }

    public void setListAllDetailPesanan(List<detailPesanan> listAllDetailPesanan) {
        this.listAllDetailPesanan = listAllDetailPesanan;
    }

    public detailPesanan getDpGeneral() {
        return dpGeneral;
    }

    public void setDpGeneral(detailPesanan dpGeneral) {
        this.dpGeneral = dpGeneral;
    }

}
