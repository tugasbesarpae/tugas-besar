/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entity.book;
import Session.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Andre
 */
@Named(value = "produkBean")
@SessionScoped
public class produkBean implements Serializable {

    //EJB Remote Instantiation
    @EJB
    private cartSessRemote cartEjb;
    @EJB
    private BookEJBRemote7 bookEjb;

    //General : pemakaian umum, ex : findAll, dsb. 
    //CRUD    : digunakan untuk proses Create, Update, Find, dan Delete.
    //----- Book -----
    //General
    private List<book> listAllBook;
    //CRUD
    public static book bookGeneral;

    public produkBean() {
        //List Instantiation
        listAllBook = new ArrayList<>();
        //General Instantiation
        bookGeneral = new book();
    }

    //Cart CRUD Methods
    //Add
    public void addItem() {
        cartEjb.addItem(bookGeneral);
    }

    //Navigasi Single Product
    public String navToSingleProduct(book boo) {
        bookGeneral = boo;
        return "singleProduct.xhtml?faces-redirect=true";
    }

    //Remove
    public void removeItem() {
        //    cartEjb.removeItem(bookGeneral);
    }

    //Hitung Total
    public Float getTotal() {
        return cartEjb.getTotal();
    }

    //Kosongkan
    public void empty() {
        cartEjb.empty();
    }

    //Book CRUD Methods
    //Read All
    public void readAllBook() {
        listAllBook = bookEjb.findBooks();
    }
    //Kosongkan
    public void emptyGeneralBook(){
        bookGeneral = new book();
    }
    //Find by Title
    public String searchProduk() {
        try {
            bookGeneral = bookEjb.findBookByTitle(bookGeneral.getTitle());
            return "singleProduct.xhtml?faces-redirect=true";
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Produk tidak diketemukan!", "Mohon masukkan judul yang benar!"));
        }
        return null;
    }

    //Show Edit
    public void showEdit(book m) {
        if (m != null) {
            bookGeneral = new book();
            bookGeneral.setId(m.getId());
            bookGeneral.setDescription(m.getDescription());
            bookGeneral.setIllustrations(m.getIllustrations());
            bookGeneral.setIsbn(m.getIsbn());
            bookGeneral.setLink(m.getLink());
            bookGeneral.setPrice(m.getPrice());
            bookGeneral.setNbOfPage(m.getNbOfPage());
            bookGeneral.setGambar(m.getGambar());
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('editMahasiswa').show();");
        }
    }

    //Show Single Product
    public String showSingleProduct(book b) {
        bookGeneral = b;
        return "singleProduct.xhtml?faces-redirect=true";
    }

    //Find
    public void findBook() {
        try {
            bookGeneral = bookEjb.findBookById(bookGeneral.getId());
            bookGeneral.getId(); //Buat pancing error kalau tidak ketemu
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau pelanggan tidak ketemu
        }
    }

    //Add
    public void addBook() {
        try {
            bookEjb.createBook(bookGeneral);
            bookGeneral = new book();
            readAllBook();
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau add nya error
        }
    }

    //Update
    public void updateBook() {
        try {
            bookEjb.updateBook(bookGeneral);
            readAllBook();
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau update nya error
        }
    }

    //Delete
    public void deleteBook() {
        try {
            findBook();
            bookEjb.deleteBook(bookGeneral);
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau delete nya error
        }
    }

    //getter-setter
    public List<book> getListAllBook() {
        return listAllBook;
    }

    public void setListAllBook(List<book> listAllBook) {
        this.listAllBook = listAllBook;
    }

    public book getBookGeneral() {
        return bookGeneral;
    }

    public void setBookGeneral(book bookGeneral) {
        this.bookGeneral = bookGeneral;
    }

}
