/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entity.pelanggan;
import Session.*;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.*;
import javax.ejb.*;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Andre
 */
@Named(value = "customerBean")
@SessionScoped
public class customerBean implements Serializable {

    //EJB Remote Instantiation
    @EJB
    private customerEJBRemote customerEjb;

    //General : pemakaian umum, ex : findAll, dsb. 
    //CRUD    : digunakan untuk proses Create, Update, Find, dan Delete.
    //----- Customer -----
    //General
    private List<pelanggan> listAllCustomer;
    //Login Data
    public static pelanggan pelangganYangLogin = new pelanggan();
    private String flagChecker;
    private String loggedChecker;
    //CRUD
    private pelanggan pelangganGeneral;

    public customerBean() {
        //List Instantiation
        listAllCustomer = new ArrayList<>();
        //General Instantiation
        pelangganYangLogin = new pelanggan();
        pelangganGeneral = new pelanggan();
    }

    //Mengecek apakah pelanggan sudah login, jika belum maka kasih placeholder
    public void cekApakahLoggedOn() {
        try {
            if (pelangganYangLogin.getNama().equals("Administrator")) {
                loggedChecker = "Administrator";
                throw new ArrayStoreException();
            } else if (pelangganYangLogin.getAlamat().isEmpty()) {
                throw new NullPointerException();
            } else {
                loggedChecker = pelangganYangLogin.getNama();
            }
        } catch (NullPointerException e) {
            //e.printStackTrace();
            loggedChecker = "Anonymous";
        } catch (ArrayStoreException es) {

        }
    }

    //Login Validator
    public String validateLogin() {
        try {
            if (pelangganYangLogin.getIdCustomer() == 0 && pelangganYangLogin.getPassword().equals("admin")) {
                pelangganYangLogin.setNama("Administrator");
                pelangganYangLogin.setAlamat(" - ");
                pelangganYangLogin.setEmail(" - ");
                pelangganYangLogin.setKota(" - ");
                pelangganYangLogin.setTelepon(0);
                cekApakahLoggedOn();
                return "/index.xhtml?faces-redirect=true";
            } else {
                pelangganYangLogin
                        = customerEjb.validatePelangganLoginDetails(pelangganYangLogin.getIdCustomer(), pelangganYangLogin.getPassword());
                cekApakahLoggedOn();
                return "/index.xhtml?faces-redirect=true";
            }
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Login gagal!", "Username atau Password Salah!"));
        }
        return null;
    }

    //Customer CRUD Methods
    //Read All
    public void readAllCustomer() {
        listAllCustomer = customerEjb.findCustomer();
    }

    //Kosongkan
    public void emptyCustomerGeneral() {
        pelangganGeneral = new pelanggan();
    }

    //Cek untuk ganti Login <--> Logout
    public void gantiCheck() {
        try {
            if (pelangganYangLogin.getAlamat().isEmpty()) {
                throw new NullPointerException();
            } else {
                flagChecker = "Logout";
            }
        } catch (NullPointerException e) {
            //e.printStackTrace();
            flagChecker = "Login";
        } catch (ArrayStoreException es) {

        }
    }

    public String navigasiCheck() {
        if (flagChecker.equals("Login")) {
            return "login.xhtml?faces-redirect=true";
        } else {
            pelangganYangLogin = new pelanggan();
            return "index.xhtml?faces-redirect=true";
        }
    }

    //Navigasi ke manage acc
    public String navToManageAcc() {
        try {
            if (pelangganYangLogin.getNama().equals("Administrator")) {
                return "manageAdmin.xhtml?faces-redirect=true";
            } else if (!loggedChecker.equalsIgnoreCase("Anonymous")) {
                return "manageAcc.xhtml?faces-redirect=true";
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Mohon lakukan login dulu!", "Mohon Login dahulu!"));
            return null;
        }

    }

    //Find
    public void findCustomer() {
        try {
            pelangganGeneral = customerEjb.findCustomerById(pelangganGeneral.getIdCustomer());
            pelangganGeneral.getIdCustomer(); //Buat pancing error kalau tidak ketemu
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau pelanggan tidak ketemu
        }
    }

    //Add
    public void addCustomer() {
        try {
            customerEjb.createCustomer(pelangganGeneral);
            pelangganGeneral = new pelanggan();
            readAllCustomer();
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau add nya error
        }
    }

    public String addCustomerRegister() {
        try {
            customerEjb.createCustomer(pelangganGeneral);
            pelangganGeneral = new pelanggan();
            readAllCustomer();
            return "login.xhtml?faces-redirect=true";
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau add nya error
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Data Login Salah", "Mohon Isi Data dengan Benar!"));
            return null;
        }
    }

    //Update 
    public void updateCustomer() {
        try {
            customerEjb.updateCustomer(pelangganGeneral);
            readAllCustomer();

        } catch (Exception e) {
            e.printStackTrace();
            //Kalau update nya error
        }
    }

    public void showEdit(pelanggan m) {
        if (m != null) {
            pelangganGeneral = new pelanggan();
            pelangganGeneral.setIdCustomer(m.getIdCustomer());
            pelangganGeneral.setAlamat(m.getAlamat());
            pelangganGeneral.setNama(m.getNama());
            pelangganGeneral.setEmail(m.getEmail());
            pelangganGeneral.setKota(m.getKota());
            pelangganGeneral.setTelepon(m.getTelepon());
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('editMahasiswa').show();");
        }
    }

    //Delete
    public void deleteCustomer() {
        try {
            findCustomer();
            customerEjb.deleteCustomer(pelangganGeneral);
            readAllCustomer();
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau delete nya error
        }
    }

    //getter-setter
    public List<pelanggan> getListAllCustomer() {
        return listAllCustomer;
    }

    public List<Integer> readListCustomerId() {
        List<Integer> hasil = new ArrayList<>();
        for (int i = 0; i < listAllCustomer.size(); i++) {
            hasil.add(listAllCustomer.get(i).getIdCustomer());
        }
        return hasil;
    }

    public void setListAllCustomer(List<pelanggan> listAllCustomer) {
        this.listAllCustomer = listAllCustomer;
    }

    public pelanggan getPelangganYangLogin() {
        return pelangganYangLogin;
    }

    public void setPelangganYangLogin(pelanggan pelangganYangLogin) {
        this.pelangganYangLogin = pelangganYangLogin;
    }

    public pelanggan getPelangganGeneral() {
        return pelangganGeneral;
    }

    public void setPelangganGeneral(pelanggan pelangganGeneral) {
        this.pelangganGeneral = pelangganGeneral;
    }

    public String getFlagChecker() {
        return flagChecker;
    }

    public void setFlagChecker(String flagChecker) {
        this.flagChecker = flagChecker;
    }

    public String getLoggedChecker() {
        return loggedChecker;
    }

    public void setLoggedChecker(String loggedChecker) {
        this.loggedChecker = loggedChecker;
    }

}
