/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entity.*;
import Session.*;
import Session.cartSessRemote;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author
 */
@Named(value = "orderBean")
@SessionScoped
public class orderBean implements Serializable {

    //EJB Remote Instantiation
    @EJB
    private orderEJBRemote orderEjb;
    @EJB
    private customerEJBRemote customerEjb;
    @EJB
    private cartSessRemote cartEjb;

    //General : pemakaian umum, ex : findAll, dsb. 
    //CRUD    : digunakan untuk proses Create, Update, Find, dan Delete.
    //----- Cart Items -----
    private List<detailPesanan> listDetailPesananDalamCart;
    //----- Pesanan -----
    //General
    private List<pesanan> listAllPesanan;
    private List<pesanan> listPesananCustomer;
    //CRUD
    private pesanan pesananGeneral;
    private List<detailPesanan> listDpPesanan;
    private int idTambah;
    
    public orderBean() {
        //List Instantiation
        listAllPesanan = new ArrayList<>();
        listPesananCustomer = new ArrayList<>();
        listDpPesanan = new ArrayList<>();
        listDetailPesananDalamCart = new ArrayList<>();
        //General Instantiation
        pesananGeneral = new pesanan();
    }

    //Cart CRUD Methods
    //Read All
    public void readAllCartPesanan() {
        listDetailPesananDalamCart = cartEjb.getListDetail();
    }

    //Kurangi Kuantitas
    public void tambahKuantitas(detailPesanan b) {
        cartEjb.addKuantitasDalamDetail(b);
        readAllCartPesanan();
        //return "cart.xhtml?faces-redirect=true";
    }

    //Tambahi Kuantitas
    public void kurangiKuantitas(detailPesanan b) {
        if (b.getKuantitas() == 1) {
            deleteCartPesanan(b);
        } else {
            cartEjb.kurangiKuantitasDalamDetail(b);
        }
        readAllCartPesanan();
        //return "cart.xhtml?faces-redirect=true";
    }

    //Add
    public String addCartPesanan(book b) {
        cartEjb.addItem(b);
        readAllCartPesanan();
        return "cart.xhtml?faces-redirect=true";
    }

    public String addCartPesananGeneral() {
        cartEjb.addItem(produkBean.bookGeneral);
        readAllCartPesanan();
        return "cart.xhtml?faces-redirect=true";
    }

    public void showEdit(pesanan m) {
        if (m != null) {
            pesananGeneral = new pesanan();
            pesananGeneral.setIdCustomer(m.getIdCustomer());
            pesananGeneral.setTotalHarga(m.getTotalHarga());
            pesananGeneral.setDaftarDetailPesanan(m.getDaftarDetailPesanan());

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('editMahasiswa').show();");
        }
    }

    //Delete
    public void deleteCartPesanan(detailPesanan b) {
        cartEjb.removeItem(b);
        readAllCartPesanan();
        //return "cart.xhtml?faces-redirect=true";
    }

    //Check Out
    public String checkOut() {
        try {
            cartEjb.getListDetail().size();
            if (customerBean.pelangganYangLogin.getNama().equals("Anonymous")) {
                throw new Exception();
            }
            cartEjb.checkout(customerBean.pelangganYangLogin);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Checkout Berhasil!", "ID Pesanan Anda adalah : "));
            return "cart.xhtml?faces-redirect=true";
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Checkout Gagal!", "Mohon Login sebelum melakukan check-out!"));
        }
        return "";
    }

    //Sub Total
    public Float readSubTotalCart(detailPesanan dp) {
        try {
            Float total = dp.getId().getPrice() * dp.getKuantitas();
            return total;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0f;
    }

    //Total Semua
    public Float readTotalTerakhirCart() {
        try {
            return cartEjb.getTotal();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0f;
    }

    //Pesanan CRUD Methods
    //Read All
    public void readAllPesanan() {
        listAllPesanan = orderEjb.findPesanan();
    }

    //Find Pesanan oleh Customer Spesifik
    public void readPesananCustomer(int id) {
        try {
            listPesananCustomer = orderEjb.findPesananCustomer(id);
        } catch (Exception e) {

        }
    }

    //Find Detail Pesanan dari Sebuah Pesanan
    public void findDetailPesanan() {
        listDpPesanan = orderEjb.findDetailDariPesanan(pesananGeneral.getIdOrder());
    }

    //Find
    public void findPesanan() {
        try {
            pesananGeneral = orderEjb.findPesananById(pesananGeneral.getIdOrder());
            pesananGeneral.getIdOrder(); //Buat pancing error kalau tidak ketemu
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau pelanggan tidak ketemu
        }
    }

    //Add
    public void addPesanan() {
        try {
            pesananGeneral.setIdCustomer(customerEjb.findCustomerById(idTambah));
            orderEjb.createPesanan(pesananGeneral);
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau add nya error
        }
    }

    //Update-
    public void updatePesanan() {
        try {
            orderEjb.updatePesanan(pesananGeneral);
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau update nya error
        }
    }

    //Delete
    public void deletePesanan() {
        try {
            findPesanan();
            orderEjb.deletePesanan(pesananGeneral);
        } catch (Exception e) {
            e.printStackTrace();
            //Kalau delete nya error
        }
    }

    //getter-setter
    public List<pesanan> getListAllPesanan() {
        return listAllPesanan;
    }

    public void setListAllPesanan(List<pesanan> listAllPesanan) {
        this.listAllPesanan = listAllPesanan;
    }

    public pesanan getPesananGeneral() {
        return pesananGeneral;
    }

    public void setPesananGeneral(pesanan pesananGeneral) {
        this.pesananGeneral = pesananGeneral;
    }

    public List<detailPesanan> getListDpPesanan() {
        return listDpPesanan;
    }

    public void setListDpPesanan(List<detailPesanan> listDpPesanan) {
        this.listDpPesanan = listDpPesanan;
    }

    public List<detailPesanan> getListDetailPesananDalamCart() {
        return listDetailPesananDalamCart;
    }

    public void setListDetailPesananDalamCart(List<detailPesanan> listDetailPesananDalamCart) {
        this.listDetailPesananDalamCart = listDetailPesananDalamCart;
    }

    public List<pesanan> getListPesananCustomer() {
        return listPesananCustomer;
    }

    public int getIdTambah() {
        return idTambah;
    }

    public void setIdTambah(int idTambah) {
        this.idTambah = idTambah;
    }

    public void setListPesananCustomer(List<pesanan> listPesananCustomer) {
        this.listPesananCustomer = listPesananCustomer;
    }

}
